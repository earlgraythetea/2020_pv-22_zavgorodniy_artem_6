/****************************************************************************
** Meta object code from reading C++ file 'directorychange.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../lab10/directorychange.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'directorychange.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DirectoryChange_t {
    QByteArrayData data[12];
    char stringdata0[184];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DirectoryChange_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DirectoryChange_t qt_meta_stringdata_DirectoryChange = {
    {
QT_MOC_LITERAL(0, 0, 15), // "DirectoryChange"
QT_MOC_LITERAL(1, 16, 15), // "fileModifiedOut"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 14), // "fileRemovedOut"
QT_MOC_LITERAL(4, 48, 14), // "addingErrorOut"
QT_MOC_LITERAL(5, 63, 16), // "removingErrorOut"
QT_MOC_LITERAL(6, 80, 16), // "logClearedLogger"
QT_MOC_LITERAL(7, 97, 15), // "logFileModified"
QT_MOC_LITERAL(8, 113, 14), // "logFileCreated"
QT_MOC_LITERAL(9, 128, 15), // "fileChangedSlot"
QT_MOC_LITERAL(10, 144, 18), // "directoryAddedSlot"
QT_MOC_LITERAL(11, 163, 20) // "directoryRemovedSlot"

    },
    "DirectoryChange\0fileModifiedOut\0\0"
    "fileRemovedOut\0addingErrorOut\0"
    "removingErrorOut\0logClearedLogger\0"
    "logFileModified\0logFileCreated\0"
    "fileChangedSlot\0directoryAddedSlot\0"
    "directoryRemovedSlot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DirectoryChange[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       3,    1,   67,    2, 0x06 /* Public */,
       4,    1,   70,    2, 0x06 /* Public */,
       5,    1,   73,    2, 0x06 /* Public */,
       6,    0,   76,    2, 0x06 /* Public */,
       7,    1,   77,    2, 0x06 /* Public */,
       8,    1,   80,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   83,    2, 0x0a /* Public */,
      10,    1,   86,    2, 0x0a /* Public */,
      11,    1,   89,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,

       0        // eod
};

void DirectoryChange::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DirectoryChange *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fileModifiedOut((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->fileRemovedOut((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->addingErrorOut((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->removingErrorOut((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->logClearedLogger(); break;
        case 5: _t->logFileModified((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->logFileCreated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->fileChangedSlot((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->directoryAddedSlot((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->directoryRemovedSlot((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DirectoryChange::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DirectoryChange::fileModifiedOut)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (DirectoryChange::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DirectoryChange::fileRemovedOut)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (DirectoryChange::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DirectoryChange::addingErrorOut)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (DirectoryChange::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DirectoryChange::removingErrorOut)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (DirectoryChange::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DirectoryChange::logClearedLogger)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (DirectoryChange::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DirectoryChange::logFileModified)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (DirectoryChange::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DirectoryChange::logFileCreated)) {
                *result = 6;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DirectoryChange::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_DirectoryChange.data,
    qt_meta_data_DirectoryChange,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DirectoryChange::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DirectoryChange::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DirectoryChange.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int DirectoryChange::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void DirectoryChange::fileModifiedOut(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DirectoryChange::fileRemovedOut(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DirectoryChange::addingErrorOut(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void DirectoryChange::removingErrorOut(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void DirectoryChange::logClearedLogger()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void DirectoryChange::logFileModified(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void DirectoryChange::logFileCreated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
