#include "directorychange.h"

DirectoryChange::DirectoryChange()
{
    w = new QFileSystemWatcher();
    log = new logFile();
    connect(w, SIGNAL(fileChanged(QString)), SLOT(fileChangedSlot(QString)));
    connect(log, SIGNAL(logFileModified(QString)), SIGNAL(logFileModified(QString)));
    connect(this, SIGNAL(logFileCreated(QString)), log, SLOT(fileChanged(QString)));
    connect(this, SIGNAL(fileModifiedOut(QString)), log, SLOT(fileModifiedOutSlot(QString)));
    connect(this, SIGNAL(fileRemovedOut(QString)), log, SLOT(fileRemovedOutSlot(QString)));
    connect(this, SIGNAL(addingErrorOut(QString)), log, SLOT(addingErrorOutSlot(QString)));
    connect(this, SIGNAL(removingErrorOut(QString)), log, SLOT(removingErrorOutSlot(QString)));
    connect(this, SIGNAL(logClearedLogger()), log, SLOT(logClearedSlot()));
}

void DirectoryChange::addPath(const QString &path) {
    QDirIterator it(path, QDirIterator::NoIteratorFlags);
    it.next();
    it.next(); // ���������� /. � /..
    while(it.hasNext()) {
        QString add = it.next();
        bool ok = w->addPath(add);
        if(ok == false)
            emit addingErrorOut(add);
    }
}

void DirectoryChange::removePath(const QString &path) {
    QDirIterator it(path, QDirIterator::NoIteratorFlags);
    it.next();
    it.next(); // ���������� /. � /..
    while(it.hasNext()) {
        QString rem = it.next();
        bool ok = w->removePath(rem);
        if(ok == false)
            emit removingErrorOut(rem);
    }
}

void DirectoryChange::fileChangedSlot(const QString& path) {
    if(w->files().contains(path))
        emit fileModifiedOut(path);
    else
        emit fileRemovedOut(path);
}

void DirectoryChange::directoryAddedSlot(const QString &path) {
    this->addPath(path);
}

void DirectoryChange::directoryRemovedSlot(const QString &path) {
    this->removePath(path);
}

QStringList DirectoryChange::getLogData() {
    return log->fileData();
}

