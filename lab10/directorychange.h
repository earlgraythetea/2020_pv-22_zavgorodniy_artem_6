#ifndef DIRECTORYCHANGE_H
#define DIRECTORYCHANGE_H

#include <QObject>
#include <QWidget>
#include <QDir>
#include <QDirIterator>
#include <QFileSystemWatcher>
#include <QString>
#include <QDebug>
#include "logfile.h"


class DirectoryChange : public QObject
{
    Q_OBJECT
public:
    DirectoryChange();
    QStringList getLogData();
protected:
    QFileSystemWatcher *w;
    logFile *log;
    void addPath(const QString &);
    void removePath(const QString &);
public slots:
    void fileChangedSlot(const QString &);
    void directoryAddedSlot(const QString &);
    void directoryRemovedSlot(const QString &);
signals:
    // �������, ��������� � logFile
    void fileModifiedOut(const QString &);
    void fileRemovedOut(const QString &);
    void addingErrorOut(const QString &);
    void removingErrorOut(const QString &);
    void logClearedLogger();
    // �������, ��������� � parent
    void logFileModified(const QString &);
    void logFileCreated(const QString &);
};

#endif // DIRECTORYCHANGE_H
