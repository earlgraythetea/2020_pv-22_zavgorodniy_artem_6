#include "logexception.h"

logException::logException(QString msg) : msg(msg)
{
}

const char* logException::what() {
    return msg.toUtf8();
}

fileNotFound::fileNotFound(QString path) {
   logException("File " + path + " not found");
}
