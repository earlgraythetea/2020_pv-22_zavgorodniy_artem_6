#ifndef LOGEXCEPTION_H
#define LOGEXCEPTION_H
#include <exception>
#include <QObject>

class logException : public std::exception
{
protected:
    QString msg;
public:
    logException(QString m="");
    const char* what();
};

class fileNotFound : public logException {
public:
    fileNotFound(QString);
};

#endif // LOGEXCEPTION_H
