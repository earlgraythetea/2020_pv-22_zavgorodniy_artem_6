#include "logfile.h"

logFile::logFile()
{
    dt = new QDateTime();
    *dt = QDateTime::currentDateTime();
}

QStringList logFile::fileData() {
    QStringList res;
    if(file->exists() && file->open(QIODevice::ReadOnly)) {
        while(!file->atEnd())
            res.append(file->readLine());
        file->close();
    } else {
        throw fileNotFound(file->fileName());
    }
    return res;
}

void logFile::write(QString msg) {
    if(file->exists() && file->open(QIODevice::ReadWrite | QIODevice::Append)) {
        file->write(msg.toUtf8());
        file->close();
        emit logFileModified(file->fileName());
    } else {
        throw fileNotFound(file->fileName());
    }
}

void logFile::fileModifiedOutSlot(const QString &path) {
    *dt = QDateTime::currentDateTime();
    QString msg = dt->date().toString("yyyy.MM.dd") + " " + dt->time().toString("hh:mm:ss") + " File " + path + " was modified\n";
    this->write(msg);
}

void logFile::fileRemovedOutSlot(const QString &path) {
    *dt = QDateTime::currentDateTime();
    QString msg = dt->date().toString("yyyy.MM.dd") + " " + dt->time().toString("hh:mm:ss") + " File " + path + " was removed\n";
    this->write(msg);
}

void logFile::addingErrorOutSlot(const QString &path) {
    *dt = QDateTime::currentDateTime();
    QString msg = dt->date().toString("yyyy.MM.dd") + " " + dt->time().toString("hh:mm:ss") + " Error while file " + path + " have been removing\n";
    this->write(msg);
}

void logFile::removingErrorOutSlot(const QString &path) {
    *dt = QDateTime::currentDateTime();
    QString msg =dt->date().toString("yyyy.MM.dd") + " " + dt->time().toString("hh:mm:ss") + " Error while file " + path + " have been removing";
    this->write(msg);
}

void logFile::fileChanged(const QString &path) {
    *dt = QDateTime::currentDateTime();
    QString msg = dt->date().toString("yyyy.MM.dd") + " " + dt->time().toString("hh:mm:ss") + " File created\n";
    file = new QFile(path);
    if(!file->exists()) {
        file->open(QIODevice::WriteOnly);
        file->close();
    }
    this->write(msg);
}

void logFile::logClearedSlot() {
    QString fname = file->fileName();
    file->remove();
    file = new QFile(fname);
    if(!file->exists()) {
        file->open(QIODevice::WriteOnly);
        file->close();
    }
    emit logFileModified(file->fileName());
}
