#ifndef LOGFILE_H
#define LOGFILE_H

#include <QObject>
#include <QFile>
#include <QDebug>
#include <QDateTime>
#include "logexception.h"

class logFile : public QObject
{
    Q_OBJECT
public:
    logFile();
    QStringList fileData();
protected:
    QFile *file;
    QDateTime *dt;
    void write(QString);
public slots:
    void fileChanged(const QString &);
    void fileModifiedOutSlot(const QString &);
    void fileRemovedOutSlot(const QString &);
    void addingErrorOutSlot(const QString &);
    void removingErrorOutSlot(const QString &);
    void logClearedSlot();
signals:
    void logFileModified(const QString&);
};

#endif // LOGFILE_H
