#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , d(new DirectoryChange)
{
    ui->setupUi(this);
    connect(this, SIGNAL(directoryAdded(QString)), d, SLOT(directoryAddedSlot(QString)));
    connect(this, SIGNAL(directoryRemoved(QString)), d, SLOT(directoryRemovedSlot(QString)));
    connect(this, SIGNAL(logCreated(QString)), d, SIGNAL(logFileCreated(QString)));
    connect(d, SIGNAL(logFileModified(QString)), SLOT(logFileModifiedSlot(QString)));
    connect(this, SIGNAL(logCleared()), d, SIGNAL(logClearedLogger()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_action_triggered()
{
    bool ok;
    QString path = QInputDialog::getText(this, "Directory adding", "Enter directory path: ",
                                         QLineEdit::Normal, "", &ok);
    if(ok)
        emit directoryAdded(path);
}

void MainWindow::on_action_2_triggered()
{
    bool ok;
    QString path = QInputDialog::getText(this, "Directory removing", "Enter directory path: ",
                                         QLineEdit::Normal, "", &ok);
    if(ok)
        emit directoryRemoved(path);
}

void MainWindow::logFileModifiedSlot(const QString &logFile) {
    ui->textBrowser->setText(d->getLogData().join('\n'));
}



void MainWindow::on_action_3_triggered()
{
    bool ok;
    QString path = QInputDialog::getText(this, "Log file creating",
                                         "Enter log file path: ",
                                         QLineEdit::Normal, "", &ok);
    if(ok)
        emit logCreated(path);
}

void MainWindow::on_action_4_triggered()
{
    emit logCleared();
}
