#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QInputDialog>
#include "directorychange.h"
#include <QTextBrowser>
#include "smartptr.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void directoryAdded(const QString &);
    void directoryRemoved(const QString &);
    void logCreated(const QString &);
    void logCleared();

private slots:
    void on_action_triggered();
    void on_action_2_triggered();
    void logFileModifiedSlot(const QString &);
    void on_action_3_triggered();
    void on_action_4_triggered();

private:
    Ui::MainWindow *ui;
    DirectoryChange *d;
};
#endif // MAINWINDOW_H
